import Link from "next/link";

export default function PreviewGrid1({ listData }) {
  const {items, contentType} = listData;
  const dir = contentType.displayName.replace(/\s+/g, '-').toLowerCase();

  return (
    <section className="max-w-5xl mx-auto my-32">
        <div className="grid grid-cols-1 mb-32 gap-y-12 gap-x-12 md:grid-cols-2">
          {items && items.map((item) => {
              return (
                <div>
                  <Link href={`/${dir}/${item.slug}`}>
                    <a>
                      <img
                        className="max-w-full mb-8 transition-shadow duration-200 bg-gray-100 rounded-lg shadow-x hover:shadow-xl"
                        src={item.fields.image.sizes.w800}
                      />
                    </a>
                  </Link>
                  <Link href={`/${dir}/${item.slug}`}>
                    <a>
                      <h2 className="mb-4 text-2xl font-bold leading-none text-gray-900 hover:underline hover:text-gray-800">
                        {item.fields.heading}
                      </h2>
                    </a>
                  </Link>
                  <Link href={`/${dir}/${item.slug}`}>
                    <a className="text-blue-500 hover:underline hover:text-blue-600">
                      Read Article
                    </a>
                  </Link>
                </div>
              );
            })}
        </div>
      </section>
  );
}
