export default function ContactSection() {
  return (
    <section id="contact-section" className="contact-form-1">
      <div className="bg-gray-500 p-10">
        <h2 className="text-2xl">Contact Form</h2>
      </div>
    </section>
  );
}
