export default function ContactDetailsSection() {
  return (
    <section id="contact-details-section" className="contact-card-1">
      <div className="bg-gray-300 p-10">
        <h2 className="text-2xl">Contact Details Section</h2>
      </div>
    </section>
  );
}
