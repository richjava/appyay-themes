export default function MediaCover2({ item }) {
  const fields = item.fields;

  return (
    <section className="px-4 py-24 bg-gray-100 services-bg-image">
     <div className="relative z-10 max-w-3xl mx-auto text-center">
        <p className="text-blue-400 uppercase tracking-widest text-sm mb-3">
          Our Services
        </p>
        <h1 className="text-4xl md:text-5xl text-gray-900 leading-none font-bold mb-10">
          {fields.heading}
        </h1>
        <p className="max-w-xl mx-auto font-light text-gray-700 leading-loose">
          {fields.blurb}
        </p>
      </div>
      <style jsx>{`
        .services-bg-image {
          background: #f7fafc url(${fields.image})
            no-repeat top center/cover;
        }

        .services-bg-image::after {
          content: "";
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
          background-color: rgba(247, 250, 252, 0.9);
        }
      `}</style>
    </section>
  );
}
