import Link from "next/link";
import { format } from "date-fns";

export default function PreviewGrid1({listData, config}) {
  const {items, contentType} = listData;
  const dir = contentType.displayName.replace(/\s+/g, '-').toLowerCase();
  const morePosts = items.slice(config.offset);

  return (
    <section className="grid grid-cols-1 mb-32 gap-y-12 gap-x-12 md:grid-cols-2">
      {morePosts.map((item) => {
        return (
          <div>
            <Link href={`/${dir}/${item.slug}`}>
              <a>
                <img
                  className="max-w-full mb-6 transition-shadow duration-200 bg-gray-100 rounded-lg shadow-x hover:shadow-xl"
                  src={item.fields.image.sizes.w800}
                />
              </a>
            </Link>
            <div className="flex mb-4">
              <p className="text-sm text-gray-600">
                {format(new Date(item.createdAt), "d LLLL yyyy")}
                <span className="mx-3 text-sm text-gray-600">|</span>
                  {item.fields.tags &&
                    item.fields.tags.map((tag) => {
                      return (
                        <Link href={`/${dir}-archive?tag=${tag}`}>
                          <a className="text-sm text-blue-500 hover:underline hover:text-blue-600">
                            {tag}
                          </a>
                        </Link>
                      );
                    })}
              </p>
            </div>
            <Link href={`/${dir}/${item.slug}`}>
              <a>
                <h2 className="mb-6 mr-4 text-3xl font-bold leading-none text-gray-900 hover:underline hover:text-gray-800">
                  {item.fields.heading}
                </h2>
              </a>
            </Link>
            <Link href={`/${dir}/${item.slug}`}>
              <a className="text-blue-500 hover:underline hover:text-blue-600">
                Read Article
              </a>
            </Link>
          </div>
        );
      })}
    </section>
  );
}
