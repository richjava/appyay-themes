import Link from "next/link";

export default function MediaCover1({item, config}) {
  const fields = item.fields;
  
  return (
    <section id="main-landing" className="media-cover-1">
      <div className="flex flex-col lg:flex-row items-center lg:h-screen bg-gray-100 -mt-20">
        <div className="flex-1 lg:order-1 mt-20 lg:mr-20 relative">
          <img
            className="relative z-10 lg:shadow-2xl lg:rounded-lg"
            src="https://source.unsplash.com/XevnZB3CdsU"
          />
          <div className="hidden lg:block bg-blue-300 rounded-lg absolute h-full w-full right-0 bottom-0 -mb-5 -mr-5"></div>
          <div className="hidden lg:block pattern-lines text-blue-600 absolute h-full w-full right-0 bottom-0 -mb-10 -mr-10"></div>
        </div>
        <div className="flex-1 px-4 mt-12 mb-20">
          <div className="sm:w-4/5 text-center lg:text-left mx-auto">
            <div className="flex items-center justify-center lg:justify-start">
              <div className="hidden lg:block w-10 border border-blue-300 mr-3 mb-3"></div>
              <p className="text-blue-400 uppercase tracking-widest text-sm mb-3">
                Preheadline Content
              </p>
            </div>
            <h2 className="text-4xl md:text-5xl lg:text-6xl text-gray-900 leading-none font-bold mb-8">
              {fields.heading}
            </h2>
            <p className="text-lg font-light text-gray-700 leading-loose xl:pr-20 mb-10 lg:mb-16">
              {fields.blurb}
            </p>
            <div className="flex flex-col sm:flex-row items-center">
              <Link href={config.cta1Href}>
                <a className="w-full xl:w-auto flex items-center justify-center px-6 py-3 rounded-md text-white border border-transparent bg-blue-400 hover:bg-blue-500 hover:border-blue-500 transition duration-200 ease-in-out mb-4 sm:mb-0 sm:mr-2">
                {config.cta1Label}
                </a>
              </Link>
              <Link href={config.cta2Href}>
                <a className="w-full xl:w-auto flex items-center justify-center px-6 py-3 rounded-md text-gray-600 border border-gray-600 hover:border-gray-900 hover:text-gray-900 transition duration-200 ease-in-out sm:ml-2">
                   {config.cta2Label}
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .pattern-lines {
          background-image: repeating-linear-gradient(
            45deg,
            currentColor 0,
            currentColor 1px,
            transparent 0,
            transparent 50%
          );
          background-size: 25px 25px;
        }
      `}</style>
    </section>
  );
}
