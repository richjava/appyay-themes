/**
 * Theme config file.
 * Includes data about use cases and their current state of development.
 * @returns Config <Object>
 */
export default function config() {
  return {
    useCases: {
      understandOffering: {
        status: "in_progress",
      },
      viewServices: {
        status: "in_progress",
      },
      viewFeatures: {
        status: "in_progress",
      },
      viewBenefits: {
        status: "in_progress",
      },
      learnAbout: {
        status: "in_progress",
      },
      makeContact: {
        status: "in_progress",
      },
    },
  };
}
